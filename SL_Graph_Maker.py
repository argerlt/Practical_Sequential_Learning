# -*- coding: utf-8 -*-
"""
Created on Wed Aug 14 16:38:37 2019

@author: gerltarc
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

def gtf(a,b,c,d,e):
    # max of this function between -8 and 8 should occur around 3.65839
    return(   np.sin(a)*np.exp(-0.100*(a-0.6)**2) + np.cos(b)*np.exp(-0.111*(b+0.6)**2)
           +((np.sin(c)*np.exp(-0.102*(c-0.4)**2))*(np.cos(d)*np.exp(-0.088*(d+0.3)**2)))+
              np.sin(e)*np.exp(-0.140*(e-0.5)**2))


plt.figure("Var A")
plt.scatter(aa,R,c=np.arange(310),cmap = cm.viridis)
plt.scatter(a[:20],gtf(a,b,c,d,e)[:20],c=np.arange(20),cmap = cm.inferno)
plt.xlabel("Variable A")
plt.ylabel("Computed Z")

plt.figure("Var B")
plt.scatter(bb,R,c=np.arange(310),cmap = cm.viridis)
plt.scatter(b[:20],gtf(a,b,c,d,e)[:20],c=np.arange(20),cmap = cm.inferno)
plt.xlabel("Variable B")
plt.ylabel("Computed Z")

plt.figure("Var C")
plt.scatter(cc,R,c=np.arange(310),cmap = cm.viridis)
plt.scatter(c[:20],gtf(a,b,c,d,e)[:20],c=np.arange(20),cmap = cm.inferno)
plt.xlabel("Variable C")
plt.ylabel("Computed Z")

plt.figure("Var D")
plt.scatter(dd,R,c=np.arange(310),cmap = cm.viridis)
plt.scatter(d[:20],gtf(a,b,c,d,e)[:20],c=np.arange(20),cmap = cm.inferno)
plt.xlabel("Variable D")
plt.ylabel("Computed Z")

plt.figure("Var E")
plt.scatter(ee,R,c=np.arange(310),cmap = cm.viridis)
plt.scatter(e[:20],gtf(a,b,c,d,e)[:20],c=np.arange(20),cmap = cm.inferno)
plt.xlabel("Variable E")
plt.ylabel("Computed Z")
