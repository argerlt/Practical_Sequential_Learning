# -*- coding: utf-8 -*-
"""
Originally Created on Mon Aug 19 16:58:32 2019
Updated Fri Feb 26 12:46:08 2021

@author: Austin Gerlt
Pure pythonic implimentation of the Sequential learning process described
in the following Citrine informatics paper:

    "High-Dimensional Materials and Process Optimization using Data-driven 
    Experimental Design with Well-Calibrated Uncertainty Estimates

    Authors:
        Julia Ling* · Maxwell Hutchinson* · Erin Antono · 
        Sean Paradiso · Bryce Meredig

Short explination:
    General idea is to have a computer attempt to map a problem space with 
    multiple inputs and outputs where the following is true:
        1) The problem space is poorly understood
        2) Existing data is too scarce to map the system reliably
        3) the cost of getting new data points is too high to map via regular
            sampling of the problem space
        4) The researchers want the "best" solution to a problem, and have a
             formula for giving a grade a set of tested inputs describing how 
             'good' it is.
    Given this, sequential learning will map a problem space, choose new data
    points that would increase the accuracy of hte mapping, run new tests, then
    repeat the process, including the new data in the mapping
"""

# Default example just maps a nonsensical equation with 600 random points

# Start with the python library imports
import numpy as np
from matplotlib import pyplot as plt
from sklearn.ensemble import RandomForestRegressor
import forestci as fci
from sklearn.datasets import make_regression
import colormaps as cm
#import righthand as rh


#Create the ground truth function (gtf) we will try to map:
def gtf(query):
    a,b,c,d,e = np.split(query,5,axis = 1)
    return(   np.sin(a)*np.exp(-0.100*(a-0.6)**2) + np.cos(b)*np.exp(-0.111*(b+0.6)**2)
           +((np.sin(c)*np.exp(-0.102*(c-0.4)**2))*(np.cos(d)*np.exp(-0.088*(d+0.3)**2)))+
              np.sin(e)*np.exp(-0.140*(e-0.5)**2))


# set number of 'trees' to use in the random forest(aka, how many people to use
# for guessing ), then create the forest. 
n_trees = 500 # < change this if you want, it can effect accuracy some as well as RAM usage
forest =  RandomForestRegressor(n_estimators=n_trees, random_state=42)


#create some initial ramdom points to test out:
data = (np.random.rand(10,5)*16)-8
# set the upper and lower bounds of where to search. 
Final_candidates = np.array([-8,8])



# make a figure of the initial function to show where the highs and lows are
plt.close('all')
plt.figure('Regression')
plt.plot((-1,4),(-1,4),color ='k')


for i in np.arange(1,10,1):
    forest.fit(data, gtf(data).ravel())
    candidates =  (np.random.rand(100000,5)*(np.max(Final_candidates)-np.min(Final_candidates)))+np.min(Final_candidates)
    estimates = forest.predict(candidates)
    mask = (estimates >= np.sort(estimates)[int(-50000/i)])
    estimates = estimates[mask]
    candidates = candidates[mask,:]
    V_IJ_unbiased = fci.random_forest_error(forest, data,candidates)
    mask = (estimates >= np.sort(estimates)[-2000])+(V_IJ_unbiased >= np.sort(V_IJ_unbiased)[-1000])
    choices = np.random.randint(0,len(mask[mask==1]),10)
    Final_candidates = candidates[choices,:]
    Final_estimates = estimates[choices]
    Final_V_IJ_unbiased = V_IJ_unbiased[choices]
    data = np.vstack([data,Final_candidates])
    Test_results = gtf(Final_candidates)
    plt.scatter(Final_estimates,Test_results,color = cm.magma(i*10),edgecolor = 'k')
    plt.errorbar(Final_estimates,Test_results,yerr=np.sqrt(Final_V_IJ_unbiased.T), fmt='o',color = cm.magma(i*10))
    plt.xlabel('Reported')
    plt.ylabel('Predicted')
    print(i,np.max(Test_results),np.max((Test_results-Final_estimates)**2),np.max(Final_candidates),np.min(Final_candidates))
    
for i in np.arange(1,10,1):
    forest.fit(data, gtf(data).ravel())
    candidates =  (np.random.rand(2000000,5)*(np.max(Final_candidates)-np.min(Final_candidates)))+np.min(Final_candidates)
    estimates = forest.predict(candidates)
    mask = (estimates >= np.sort(estimates)[int(-2000/i)])
    estimates = estimates[mask]
    candidates = candidates[mask,:]
    V_IJ_unbiased = fci.random_forest_error(forest, data,candidates)
    mask = (estimates >= np.sort(estimates)[-150])+(V_IJ_unbiased >= np.sort(V_IJ_unbiased)[-50])
    choices = np.random.randint(0,len(mask[mask==1]),10)
    Final_candidates = candidates[mask,:][choices,:]
    Final_estimates = estimates[mask][choices]
    Final_V_IJ_unbiased = V_IJ_unbiased[mask][choices]
    data = np.vstack([data,Final_candidates])
    Test_results = gtf(Final_candidates)
    plt.scatter(Final_estimates,Test_results,color = cm.magma(i*10),edgecolor = 'k')
    plt.errorbar(Final_estimates,Test_results,yerr=np.sqrt(Final_V_IJ_unbiased.T), fmt='o',color = cm.magma(110+(i*10)))
    plt.xlabel('Reported')
    plt.ylabel('Predicted')
    print(i+10,np.max(Test_results),np.max((Test_results-Final_estimates)**2),np.max(Final_candidates),np.min(Final_candidates))
    
for i in np.arange(1,10,1):
    forest.fit(data, gtf(data).ravel())
    candidates =  (np.random.rand(2000000,5)*(np.max(Final_candidates)-np.min(Final_candidates)))+np.min(Final_candidates)
    estimates = forest.predict(candidates)
    mask = (estimates >= np.sort(estimates)[int(-200/i)])
    estimates = estimates[mask]
    candidates = candidates[mask,:]
    choices = np.random.randint(0,len(mask[mask==1]),10)
    Final_candidates = candidates[choices,:]
    Final_estimates = estimates[choices]
    data = np.vstack([data,Final_candidates])
    Test_results = gtf(Final_candidates)
    plt.scatter(Final_estimates,Test_results,color = cm.magma(i*10),edgecolor = 'k')
    plt.errorbar(Final_estimates,Test_results,yerr=np.sqrt(Final_V_IJ_unbiased.T), fmt='o',color = cm.magma(220+(i*10)))
    plt.xlabel('Reported')
    plt.ylabel('Predicted')
    print(i+20,np.max(Test_results),np.max((Test_results-Final_estimates)**2),np.max(Final_candidates),np.min(Final_candidates))

