# -*- coding: utf-8 -*-
"""
Created on Wed Mar 24 03:01:38 2021

Pure pythonic implimentation of the Sequential learning process described
in the following Citrine informatics paper:

    "High-Dimensional Materials and Process Optimization using Data-driven 
    Experimental Design with Well-Calibrated Uncertainty Estimates

    Authors:
        Julia Ling* · Maxwell Hutchinson* · Erin Antono · 
        Sean Paradiso · Bryce Meredig

Short explination:
    General idea is to have a computer attempt to map a problem space with 
    multiple inputs and outputs where the following is true:
        1) The problem space is poorly understood
        2) Existing data is too scarce to map the system reliably
        3) the cost of getting new data points is too high to map via regular
            sampling of the problem space
        4) The researchers want the "best" solution to a problem, and have a
             formula for giving a grade a set of tested inputs describing how 
             'good' it is.
    Given this, sequential learning will map a problem space, choose new data
    points that would increase the accuracy of hte mapping, run new tests, then
    repeat the process, including the new data in the mapping
"""

# Start with the python library imports
import numpy as np
import time
from matplotlib import pyplot as plt
from sklearn.ensemble import RandomForestRegressor
import forestci as fci
from sklearn.datasets import make_regression
import colormaps as cm



#Create the ground truth function (gtf) we will try to map:
def ground_truth_function(query):
    a,b,c,d,e = np.split(query,5,axis = 1)
    return(   np.sin(a)*np.exp(-0.100*(a-0.6)**2) + np.cos(b)*np.exp(-0.111*(b+0.6)**2)
           +((np.sin(c)*np.exp(-0.102*(c-0.4)**2))*(np.cos(d)*np.exp(-0.088*(d+0.3)**2)))+
              np.sin(e)*np.exp(-0.140*(e-0.5)**2))



# Bunch of functions to simplify life
def make_forest(number_of_trees,random_state='random'):
    """Helper function for randomly generating forest with N trees
(Maybe add more control here later, such as size/complexity/depth?)"""
    if random_state == 'random':
    # pass in your own state if you want identical starting points
    # Otherwise this will just make the state random
        rs = int(time.time())%1111
    else:
        try:
            rs = int(random_state)
        except:
            rs = 42
    return(RandomForestRegressor(number_of_trees,random_state = rs))

def refit_forest(forest,inputs,outputs):
    return(forest.fit(inputs,outputs))

def predict_random_querys(forest,inputs,upper_bounds,lower_bounds, guess_count = 10e4):
    random_seeds = np.random.rand(int(guess_count)*len(upper_bounds)).reshape(int(guess_count),len(upper_bounds))
    querys = random_seeds*(upper_bounds-lower_bounds)+(lower_bounds)
    estimates = forest.predict(querys)
    variance = fci.random_forest_error(forest, inputs,querys)
    query_data = [querys, estimates, variance]
    return(query_data)

def predict_specific_querys(forest, inputs, querys):
    estimates = forest.predict(querys)
    variance = fci.random_forest_error(forest, inputs,querys)
    query_data = [querys, estimates, variance]
    return(query_data)

def choose_experiments_from_querys(query_data, N, exploration_weight = 10,exploitation_weight = 10,random_weight = 1):
    querys = query_data[0]
    estimates = query_data[1]
    variance  = query_data[2]
    normed_est = estimates/np.max(estimates)
    normed_var = variance/np.max(variance)
    value_weight= (exploitation_weight)*(-np.log(1.00004-normed_est))
    var_weight  = (exploration_weight )*(-np.log(1.00004-normed_var))
    rand_weight = np.random.rand(querys.shape[0])*10
    weights =  value_weight+var_weight+rand_weight
    cutoff = np.sort(weights)[-int(N)]
    mask = (weights >cutoff)
    experiment_data =[querys[mask], estimates[mask], variance[mask],weights[mask]]
    return(experiment_data)



# Make an initial forest
forest = make_forest(500)
#create some initial ramdom points to test out and get their real value:
inputs = (np.random.rand(10,5)*16)-8
outputs = ground_truth_function(inputs)
# set the upper and lower bounds of where to search. 
upper_bounds = np.array([ 8, 8, 8, 8, 8])
lower_bounds = np.array([-8,-8,-8,-8,-8])



for i in np.arange(10):
    forest = refit_forest(forest, inputs, outputs)
    query_data = predict_random_querys(forest,inputs,upper_bounds,lower_bounds)
    experiment_data = choose_experiments_from_querys(query_data,10)
    new_experimental_inputs = experiment_data[0]
    new_experimental_outputs = ground_truth_function(new_experimental_inputs)
    inputs =  np.vstack([inputs,new_experimental_inputs])
    outputs = np.vstack([outputs,new_experimental_outputs])
    print(i,np.max(new_experimental_outputs),np.max((new_experimental_outputs-experiment_data[1])**2))

    
    
# # make a figure of the initial function to show where the highs and lows are
# plt.close('all')
# plt.figure('Regression')
# plt.plot((-1,4),(-1,4),color ='k')


# for i in np.arange(1,10,1):
#     forest.fit(data, gtf(data).ravel())
#     candidates =  (np.random.rand(100000,5)*(np.max(Final_candidates)-np.min(Final_candidates)))+np.min(Final_candidates)
#     estimates = forest.predict(candidates)
#     mask = (estimates >= np.sort(estimates)[int(-50000/i)])
#     estimates = estimates[mask]
#     candidates = candidates[mask,:]
#     V_IJ_unbiased = fci.random_forest_error(forest, data,candidates)
#     mask = (estimates >= np.sort(estimates)[-2000])+(V_IJ_unbiased >= np.sort(V_IJ_unbiased)[-1000])
#     choices = np.random.randint(0,len(mask[mask==1]),10)
#     Final_candidates = candidates[choices,:]
#     Final_estimates = estimates[choices]
#     Final_V_IJ_unbiased = V_IJ_unbiased[choices]
#     data = np.vstack([data,Final_candidates])
#     Test_results = gtf(Final_candidates)
#     plt.scatter(Final_estimates,Test_results,color = cm.magma(i*10),edgecolor = 'k')
#     plt.errorbar(Final_estimates,Test_results,yerr=np.sqrt(Final_V_IJ_unbiased.T), fmt='o',color = cm.magma(i*10))
#     plt.xlabel('Reported')
#     plt.ylabel('Predicted')
#     print(i,np.max(Test_results),np.max((Test_results-Final_estimates)**2),np.max(Final_candidates),np.min(Final_candidates))
    

# for i in np.arange(1,10,1):
#     forest.fit(data, gtf(data).ravel())
#     candidates =  (np.random.rand(2000000,5)*(np.max(Final_candidates)-np.min(Final_candidates)))+np.min(Final_candidates)
#     estimates = forest.predict(candidates)
#     mask = (estimates >= np.sort(estimates)[int(-2000/i)])
#     estimates = estimates[mask]
#     candidates = candidates[mask,:]
#     V_IJ_unbiased = fci.random_forest_error(forest, data,candidates)
#     mask = (estimates >= np.sort(estimates)[-150])+(V_IJ_unbiased >= np.sort(V_IJ_unbiased)[-50])
#     choices = np.random.randint(0,len(mask[mask==1]),10)
#     Final_candidates = candidates[mask,:][choices,:]
#     Final_estimates = estimates[mask][choices]
#     Final_V_IJ_unbiased = V_IJ_unbiased[mask][choices]
#     data = np.vstack([data,Final_candidates])
#     Test_results = gtf(Final_candidates)
#     plt.scatter(Final_estimates,Test_results,color = cm.magma(i*10),edgecolor = 'k')
#     plt.errorbar(Final_estimates,Test_results,yerr=np.sqrt(Final_V_IJ_unbiased.T), fmt='o',color = cm.magma(110+(i*10)))
#     plt.xlabel('Reported')
#     plt.ylabel('Predicted')
#     print(i+10,np.max(Test_results),np.max((Test_results-Final_estimates)**2),np.max(Final_candidates),np.min(Final_candidates))
    
# for i in np.arange(1,10,1):
#     forest.fit(data, gtf(data).ravel())
#     candidates =  (np.random.rand(2000000,5)*(np.max(Final_candidates)-np.min(Final_candidates)))+np.min(Final_candidates)
#     estimates = forest.predict(candidates)
#     mask = (estimates >= np.sort(estimates)[int(-200/i)])
#     estimates = estimates[mask]
#     candidates = candidates[mask,:]
#     choices = np.random.randint(0,len(mask[mask==1]),10)
#     Final_candidates = candidates[choices,:]
#     Final_estimates = estimates[choices]
#     data = np.vstack([data,Final_candidates])
#     Test_results = gtf(Final_candidates)
#     plt.scatter(Final_estimates,Test_results,color = cm.magma(i*10),edgecolor = 'k')
#     plt.errorbar(Final_estimates,Test_results,yerr=np.sqrt(Final_V_IJ_unbiased.T), fmt='o',color = cm.magma(220+(i*10)))
#     plt.xlabel('Reported')
#     plt.ylabel('Predicted')
#     print(i+20,np.max(Test_results),np.max((Test_results-Final_estimates)**2),np.max(Final_candidates),np.min(Final_candidates))

